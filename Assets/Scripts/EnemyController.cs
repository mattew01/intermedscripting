﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour
{
	public bool canMove = false;

	public GameObject player;
	public float attackRange = 10.0f;
	public Transform firePoint;
	public GameObject prefabProjectile; // the bullet/laser/etc

	public float fireRate = 1.0f; // how often it fires
	private float nextFire;
	public bool doesNotDisengage = false; // after finding the player, don't stop attacking.
	private bool playerFound = false; // has the player been found

	private float damping = 6.0f;
	public float movementSpeed = 0.1f;

	private Vector3 targetDirection;
	private float targetAngle;
	private Rigidbody2D thisRigidbody;

	// Use this for initialization
	void Start ()
	{
		playerFound = false;

		thisRigidbody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (player) {
			if (attackRange > Vector3.Distance (player.transform.position,
			                                   	transform.position) 
				|| playerFound) 
			{
				// we found the player & we don't ever want to stop attacking
				if (doesNotDisengage) {
					playerFound = true;
				}
				// look at the player
				targetDirection = player.transform.position - transform.position;
				targetAngle = Mathf.Atan2 (targetDirection.y, targetDirection.x) * Mathf.Rad2Deg;
				transform.rotation = Quaternion.AngleAxis ( targetAngle, Vector3.forward );

				// movement force
				move( );

				// is it able to fire
				if (Time.time > nextFire) {
					nextFire = Time.time + fireRate;
					GameObject bullet = Instantiate (prefabProjectile, 
					                                 firePoint.position,
					                                 firePoint.rotation) as GameObject;
					bullet.GetComponent<Projectile>().CreatedBy ("Enemy");
				}
			} else {
				// when not in range 
				patrol();
			}
		} else {
			player = GameObject.FindGameObjectWithTag ("Player");
		}
	}

	public Transform[] wayPoints;
	public int currentWP; // current wayPoint it is traveling to
	public float distanceToWayPoint = 1.0f; // we're close enough

	private void patrol() {
		if (wayPoints.Length == 0) { // nothing in the array, don't do anything
			return;
		}
		if (wayPoints [currentWP] == null) {
			nextWayPoint();
			return;
		}

		if (distanceToWayPoint > Vector3.Distance (wayPoints [currentWP].position,
		                                           	transform.position)) {
			nextWayPoint ();
		} else {
			transform.LookAt ( wayPoints[currentWP] );
			// add force or speed
		}

	}

	private void nextWayPoint()
	{
		currentWP++;
		if (currentWP > wayPoints.Length - 1) {
			currentWP = 0;
		}
	}

	public float maxSpeed = 1.0f; //the max speed of the enemy
	public float moveForce = 50.0f; 

	private void move( )
	{
		if (! canMove) return; // exit if we can't move

		if ( Mathf.Abs(thisRigidbody.velocity.x) < maxSpeed &&
		     Mathf.Abs(thisRigidbody.velocity.y) < maxSpeed) 
		{
			thisRigidbody.AddForce( transform.right * moveForce );
		}

	}

}







