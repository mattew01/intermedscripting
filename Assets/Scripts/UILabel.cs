﻿using UnityEngine;
using System.Collections;

public class UILabel : UIElement {

	public string Text;

	void OnGUI() {
		uiConfig ();

		GUI.Label ( elementRect, Text );

	}
}
