﻿using UnityEngine;
using System.Collections;

public class WeaponController : MonoBehaviour {

	public Transform firePoint;
	public GameObject projectilePrefab;
	
	private float nextFire = 0.0f;
	public float fireRate = 1.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Time.time > nextFire)
		{
			if( Input.GetMouseButton ( 0 ) || Input.GetKey ( KeyCode.Space ) )
			{
				nextFire = Time.time + fireRate;
				fireWeapon();
			}
		}
	}
	
	// FIRE THE WEAPON!
	private void fireWeapon()
	{
		if (firePoint)
		{
			GameObject proj = Instantiate ( projectilePrefab, 
			                               firePoint.position, 
			                               firePoint.rotation ) as GameObject;
			proj.GetComponent<Projectile>().CreatedBy("Player");
		}
	}
}
