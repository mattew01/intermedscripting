﻿using UnityEngine;
using System.Collections;

public class Joystick : MonoBehaviour {

	public float maxSpeed;

	private playerMoveWASD playerMove;
	private Rigidbody2D thisRigidbody;
	// Update is called once per frame

	void Awake() {
		playerMove = GameObject.FindObjectOfType<playerMoveWASD> ();
		if (playerMove == null)
			Debug.LogWarning ("Unable to find PlayerMoveWASD");
		thisRigidbody = this.GetComponent<Rigidbody2D> ();
	}


//#if UNITY_ANDROID || UNITY_IOS
	void FixedUpdate () {
		Vector2 delta = Vector2.zero;
		// get joystick input

		gatherJoystickInput (ref delta);
		delta *= maxSpeed;
		thisRigidbody.velocity = new Vector2(delta.x, delta.y);
	
	}
//#endif

	private void gatherJoystickInput( ref Vector2 delta )
	{
		int count = Input.touchCount;

		if (count == 0)
			resetJoystick ();
		else {

		}
	}


	private void resetJoystick( )
	{

	}
}
