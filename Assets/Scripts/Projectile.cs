﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	public float damage;
	public float fireRate;
	private string creator; // who created this projectile
	public float speed;
	public float lifetime = 5.0f;

	// Use this for initialization
	void Start () {
		rigidbody2D.velocity = transform.right * speed;

		Destroy (this.gameObject, lifetime);
	}
	
	void OnTriggerEnter2D( Collider2D other ) {
		if (other.tag == "Player" && creator == "Enemy") {
			// deal the damage to the player
			//Debug.Log ("Hit Player");
			GameController.Instance.AddPoints( 1.0f );
			Destroy ( this.gameObject ); // destroy itself

		} else if (other.tag == "Enemy" && creator == "Player") {

			// deal the damage to the enemy
			//Debug.Log ("Hit Enemy");
			GameController.Instance.AddPoints( 1.0f );
			Destroy ( this.gameObject );
		}
	}

	public void CreatedBy( string tag ) {
		creator = tag;
	}

	public float FireRate() {
		return fireRate;
	}
}
